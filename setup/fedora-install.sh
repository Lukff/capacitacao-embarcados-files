#!/bin/bash

# Update system
sudo dnf upgrade -y

if [[ ! -f to-install.cfg ]]; then
    echo "Creating empty list of packages \"to-install.cfg\""
    > to-install.cfg
    cat<<-EOF >> to-install.cfg
	# Put in this file the packages you want to install
	# Lines containing a '#' character are not read
	EOF
else
    # Setup rpmfusion
    #RPMFSFREE="https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm"
    #RPMFSNFREE="https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm"

    #sudo dnf install -y "$RPMFSFREE" "$RPMFSNFREE"

    # Setup flash player
    #FLSH="http://linuxdownload.adobe.com/adobe-release/adobe-release-x87_64-1.0-1.noarch.rpm"
    
    #sudo dnf install -y "$FLSH"
    #sudo dnf install -y flash-player-ppapi
    #sudo dnf install -y freshplayerplugin

    # Install packages
    cat to-install.cfg | grep -v \# | xargs sudo dnf install -y
fi
